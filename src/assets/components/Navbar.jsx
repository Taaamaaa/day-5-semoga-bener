import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import React from "react";
import { useNavigate } from "react-router-dom";

const Navbar = ({ activePage, handleBackToHome, handleHobbyClick }) => {
  const navigate = useNavigate();
  return (
    <nav className="headerContent">
      <Grid container alignItems="center" display="flex">
        <Grid item>
          <Typography
            variant="h6"
            color="textPrimary"
            style={{
              color: "white",
              textDecoration: "none",
              marginRight: "10px",
              borderBottom: activePage === "myapp" ? "2px solid white" : "none",
            }}
            onClick={() => navigate("/")}
          >
            My App
          </Typography>
        </Grid>
        <Grid item display="flex">
          <Typography
            variant="h6"
            color="textPrimary"
            style={{
              color: "white",
              textDecoration: "none",
              marginRight: "10px",
              borderBottom: activePage === "about" ? "2px solid white" : "none",
            }}
            onClick={() => navigate("/about")}
          >
            About
          </Typography>
          <Typography
            variant="h6"
            color="textPrimary"
            style={{
              color: "white",
              textDecoration: "none",
              borderBottom: activePage === "hobby" ? "2px solid white" : "none",
            }}
            onClick={handleHobbyClick}
          >
            Hobby
          </Typography>
        </Grid>
      </Grid>
    </nav>
  );
};

export default Navbar;
