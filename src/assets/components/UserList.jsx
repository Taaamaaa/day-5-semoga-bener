import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import Search from "./Search";
import { useNavigate } from "react-router-dom";
import Navbar from "./Navbar";

const UserList = ({ userList, handleEdit, handleSearch }) => {
  const [selectedUser, setSelectedUser] = useState(null);
  const [open, setOpen] = useState(false);
  const [isBlur, setIsBlur] = useState(false);
  const navigate = useNavigate();

  const handleEditUser = (user) => {
    handleEdit(user);
  };

  const handleViewUser = (user) => {
    navigate(`/user/${userList.indexOf(user)}`, { state: { user } });
    setOpen(true);
    setIsBlur(true);
  };

  const handleClose = () => {
    setOpen(false);
    setIsBlur(false);
  };

  const clearSelectedUser = () => {
    setSelectedUser(null);
  };

  return (
    <div className={`contentBoxUser ${isBlur ? "blur" : ""}`}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={12} sm={6}>
          <Search handleSearch={handleSearch} padding={5} />
        </Grid>
      </Grid>
      <div className="userBox">
        {userList.map((user, index) => (
          <div className="userData" key={index}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <div className="userLeft">
                  <div>
                    <Typography variant="body1">Name: {user.name}</Typography>
                  </div>
                  <div>
                    <Typography variant="body1">
                      Address: {user.address}
                    </Typography>
                  </div>
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <div className="userRight">
                  <Typography variant="body1">Hobby: {user.hobby}</Typography>
                  <Grid display="flex">
                    <Button
                      variant="contained"
                      size="small"
                      sx={{ width: "80px", marginRight: "5px" }}
                      onClick={() => handleViewUser(user)}
                    >
                      View
                    </Button>
                    <Button
                      variant="contained"
                      size="small"
                      sx={{ width: "80px" }}
                      onClick={() => handleEditUser(user)}
                    >
                      Edit
                    </Button>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </div>
        ))}
      </div>

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>View User</DialogTitle>
        {selectedUser && (
          <DialogContent>
            <Typography variant="body1">Name: {selectedUser.name}</Typography>
            <Typography variant="body1">
              Address: {selectedUser.address}
            </Typography>
            <Typography variant="body1">Hobby: {selectedUser.hobby}</Typography>
          </DialogContent>
        )}
        <DialogActions>
          <Button onClick={handleClose} variant="contained" size="small">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default UserList;
