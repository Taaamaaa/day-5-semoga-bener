import React from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";

const AddUserDialog = ({
  open,
  handleClose,
  handleSave,
  handleCancel,
  name,
  address,
  hobby,
  handleNameChange,
  handleAddressChange,
  handleHobbyChange,
  nameError,
  addressError,
  hobbyError,
}) => {
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Add User</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          label="Name"
          type="text"
          fullWidth
          value={name}
          onChange={handleNameChange}
          error={nameError}
          helperText={nameError && "Name cannot be empty"}
        />
        <TextField
          margin="dense"
          label="Address"
          type="text"
          fullWidth
          value={address}
          onChange={handleAddressChange}
          error={addressError}
          helperText={addressError && "Address cannot be empty"}
        />
        <TextField
          margin="dense"
          label="Hobby"
          type="text"
          fullWidth
          value={hobby}
          onChange={handleHobbyChange}
          error={hobbyError}
          helperText={hobbyError && "Hobby cannot be empty"}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel} color="primary">
          Cancel
        </Button>
        <Button onClick={handleSave} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddUserDialog;
