import React from "react";
import { useLocation } from "react-router-dom";

const Hobby = () => {
  const location = useLocation();
  const hobbies = location.state?.hobbies;

  return (
    <div>
      <h1>Hobby Page</h1>
      <h2>All Hobbies:</h2>
      <ul>
        {hobbies.map((hobby, index) => (
          <li key={index}>{hobby}</li>
        ))}
      </ul>
    </div>
  );
};
export default Hobby;
