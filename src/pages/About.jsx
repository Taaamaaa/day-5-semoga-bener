import React from "react";
import Navbar from "../assets/components/Navbar";
const About = () => {
  return (
    <div>
      <h1>About</h1>
      <p>This is an application designed to manage user information.</p>
      <p>It allows you to add, edit, and view user details.</p>
      <p>
        You can also search for users based on their name, address, or hobby.
      </p>
      <p>Feel free to explore the different features and functionalities!</p>
    </div>
  );
};

export default About;
