import React from "react";
import { useLocation } from "react-router-dom";
import Navbar from "../assets/components/Navbar";
const UserView = () => {
  const location = useLocation();
  const { user } = location.state;

  return (
    <div>
      <Navbar />
      <h2>User Details</h2>
      <p>Name: {user.name}</p>
      <p>Address: {user.address}</p>
      <p>Hobby: {user.hobby}</p>
    </div>
  );
};

export default UserView;
