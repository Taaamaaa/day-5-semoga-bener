import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Homepage from "./pages/Homepage";
import Hobby from "./pages/Hobby";
import About from "./pages/About";
import UserView from "./pages/UserView";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/hobby" element={<Hobby />} />
        <Route path="/about" element={<About />} />
        <Route path="/user/:index" element={<UserView />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
